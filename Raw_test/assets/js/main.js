(function ($) {

    window.onload = function () {
        galerry_product();
        $(document).ready(function () {		
			multi_Up_load();
           
        });        
    };

})(jQuery);




function multi_Up_load(){
	var inputs = document.querySelectorAll('.inputfile');
	if(inputs == null){
		return 0;
	}
	else{
		Array.prototype.forEach.call( inputs, function( input )
	{
		var label	 = document.querySelector("#file-name"),
			labelVal = label.innerHTML;

		input.addEventListener( 'change', function( e )
		{
			var fileName = '';
			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				label.innerHTML = fileName;
			else
				label.innerHTML = labelVal;
		});
	});
	}
}

function galerry_product(){
    $('.slider-thumb').slick({
		autoplay: true,
		vertical: true,
        margin: 0,
		infinite: true,
		verticalSwiping: true,
		slidesPerRow: 1,
		slidesToShow: 6,
		asNavFor: '.slider-preview',
		focusOnSelect: true,
		prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-up"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-down"></i></button>',
        centerMode: true,
		responsive: [
			{
				breakpoint: 767,
				settings: {
                    vertical: false,
                    slidesToShow: 5,
				}
			},
			{
				breakpoint: 479,
				settings: {
					vertical: false,
					slidesPerRow: 1,
					slidesToShow: 3,
				}
			},
		]
	});
	$('.slider-preview').slick({
		autoplay: true,
		vertical: true,
		infinite: true,
		slidesPerRow: 1,
		slidesToShow: 1,
		asNavFor: '.slider-thumb',
		arrows: false,
        draggable: true,
        autoplaySpeed: 5000,
        verticalSwiping: true,
        
		responsive: [
			{
				breakpoint: 767,
				settings: {
					vertical: false,
					verticalSwiping: false,
				}
			},
		]
	});
}
